import unittest
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab6FunctionalTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		
	def tearDown(self):
		self.selenium.implicitly_wait(5)
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()
		
	def test_input_status(self):
		selenium = self.selenium
		#Opening the link we want to test
		selenium.get('http://ppw-adeliaut.herokuapp.com/lab-6/')
		
		#Find the form element
		status = selenium.find_element_by_name('my_status')
		submit = selenium.find_element_by_class_name('btn')
		
		#Fill the form with data
		status.send_keys("Coba Coba")
		time.sleep(5)
        
		#Submitting the form
		submit.send_keys(Keys.RETURN)
		
		#Check if status is successfully added
		self.assertIn("Coba Coba", self.selenium.page_source)
		
	def test_layout_position(self):
		selenium = self.selenium
		selenium.get('http://ppw-adeliaut.herokuapp.com/lab-6/')
		
		hello_location = (selenium.find_element_by_xpath("//body//h2//center//b")).location
		form_location = (selenium.find_element_by_xpath("//body//center//form//p//textarea")).location
		self.assertEqual({'x': 363, 'y': 66}, hello_location)
		self.assertEqual({'x': 319, 'y': 115}, form_location)
	
	def test_used_the_right_css(self):
		selenium = self.selenium
		selenium.get('http://ppw-adeliaut.herokuapp.com/lab-6/')
		
		hello_title = selenium.find_element_by_xpath("//body//h2//center//b")
		my_status_title = selenium.find_element_by_xpath("//body//section[@id='my-list']//div[@class='container']//h2[@class='my-list-title']//center//b")
		
		self.assertTrue("Hello, Apa kabar?:)", hello_title.text)
		self.assertTrue("My Status", my_status_title.text)
		
	def test_change_theme_on_click_button(self):
		selenium = self.selenium
		selenium.get('http://ppw-adeliaut.herokuapp.com/lab-6/my-profile')
		
		time.sleep(3)
		change_theme = selenium.find_element_by_id("theme").click()
		time.sleep(10)
		
if __name__ == '__main__':
    unittest.main(warnings='ignore')