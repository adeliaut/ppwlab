from django.db import models

class Subscribe(models.Model):
	email=models.EmailField(unique=True)
	name=models.CharField(max_length=50)
	password=models.CharField(max_length=30)