from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from .forms import SubscribeForm
from .models import Subscribe
import requests, json, re

from django.contrib.auth import authenticate, login

@csrf_exempt
def subscribe(request):
	data = []
	if request.method == "POST":
		form = SubscribeForm(request.POST or None)
		email = request.POST['email']
		name = request.POST['name']
		password = request.POST['password']
		filter_email = Subscribe.objects.filter(email=email)
		if (len(filter_email) > 0):
			response_message = 'Email has been taken.'
		else:
			new_subscriber = Subscribe(email=email, name=name, password=password)
			new_subscriber.save()
	else:
		form = SubscribeForm()
	return render(request, 'subscribe.html', {'form': form})
	
@csrf_exempt
def unsubscribe(request):
	subId = request.POST['id']
	Subscribe.objects.filter(id=subId).delete()
	return HttpResponse(json.dumps({'message': "Errrr, something goes wrong.."}),
									content_type="application/json")

def model_to_dict(obj):
	data = serializers.serialize('json',[obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data
								
@csrf_exempt
def validation(request):
	email_validation= False
	name_validation= False
	password_validation= False
	
	if request.method == "POST":
		email = request.POST['email']
		name = request.POST['name']
		password = request.POST['password']
		
		if 5 <= len(name) <= 50:
			name_validation = True
			
		if len(email) >= 2:
			email_validation = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email))
			
		if 8 <= len(password) <= 30:
			password_validation = True
			
		if not name_validation:
			return HttpResponse(json.dumps({'message': 'Please lengthen your name to 5 characters and less than 50.'}), 
											content_type="application/json")
			
		if not email_validation:
			return HttpResponse(json.dumps({'message': 'Please write your email correctly.'}), 
											content_type="application/json")
			
		filter_email = Subscribe.objects.filter(email=email)
		if len(filter_email) > 0:
			return HttpResponse(json.dumps({'message': 'Email has been taken..'}), 
											content_type="application/json")
			
		if not password_validation:
			return HttpResponse(json.dumps({'message': 'Please lengthen your password to 8 characters and less than 30.'}), 
											content_type="application/json")
			
		return HttpResponse(json.dumps({'message': 'Your datas are valid!'}), 
										content_type="application/json")
		
	else:
		return HttpResponse(json.dumps({'message': "ciye error"}), 
							content_type="application/json")
							
def login (request):
	return render(request, 'log_in.html')

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username=username, password=password)
	if user is not None:
		login(request, user)
		return render(request, 'book_list.html')
	else:
		return render(request, 'log_in.html')
		