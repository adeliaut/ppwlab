from django.conf.urls import url
from .views import subscribe, validation, unsubscribe, login, my_view
app_name = "Lab10"

urlpatterns = [
	url(r'subscribe/$', subscribe, name = 'subscribe'),
	url(r'validation/$', validation, name = 'validation'),
	url(r'unsubscribe/$', unsubscribe, name = 'unsubscribe'),
	url(r'log-in/$', login, name = 'login'),
	url(r'success/$', my_view, name = 'success'),
	
]