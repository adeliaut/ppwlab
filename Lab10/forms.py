from django import forms
from .models import Subscribe

class SubscribeForm(forms.ModelForm):
	email = forms.CharField(widget=forms.TextInput(attrs={
		'type'			: 'email',
		'class'			: 'form-control',
		'placeholder'	: 'Email',
		'id'			: 'email_field',
	}))	
	
	name = forms.CharField(widget=forms.TextInput(attrs={
		'class'			: 'form-control',
		'placeholder'	: 'Name',
		'id'			: 'name_field',
	}), max_length=50)	
	
	password = forms.CharField(widget=forms.TextInput(attrs={
		'type'			: 'password',
		'class'			: 'form-control',
		'placeholder'	: 'Password',
		'id'			: 'pass_field',
	}), max_length=30)
	
	class Meta:
		model = Subscribe
		fields = ['email', 'name', 'password']