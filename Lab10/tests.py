from django.test import TestCase, Client
from django.urls import resolve

from .views import subscribe, validation
from .models import Subscribe
from .forms import SubscribeForm

import json

class Lab6UnitTest(TestCase):
	def test_lab_10_subscribe_page_url_is_exist(self):
		response = Client().get('/lab-10/subscribe/')
		self.assertEqual(response.status_code, 200)		
	
	def test_lab_10_validation_page_url_is_exist(self):
		response = Client().get('/lab-10/validation/')
		self.assertEqual(response.status_code, 200)	

	def test_lab_10_subscribe_page_using_subscribe_html(self):
		response = Client().get('/lab-10/subscribe/')
		self.assertTemplateUsed(response, 'subscribe.html')	

	def test_lab_10_using_subscribe_func(self):
		found = resolve('/lab-10/subscribe/')
		self.assertEqual(found.func, subscribe)	
		
	def test_lab_10_using_validaition(self):
		found = resolve('/lab-10/validation/')
		self.assertEqual(found.func, validation)	

	def test_lab_10_models_can_create_new_subscriber(self):
		new_subscriber = Subscribe.objects.create(
					email='adel@mail.com',
					name='adel',
					password='omaewa',)
		counting_all_available_new_subscriber = Subscribe.objects.all().count()
		self.assertEqual(counting_all_available_new_subscriber, 1)
	
	def test_lab_10_can_save_a_POST_request(self):
		response = self.client.post('/lab-10/subscribe/', data = {
				'email' 	: 'sehat@amin.com',
				'name' 		: 'anak pacil',
				'password'	: 'aamiin...',
				})
		counting_all_available_subscriber = Subscribe.objects.all().count()
		self.assertEqual(counting_all_available_subscriber, 1)
		
	# def test_lab_10_validation_email(self):
		# data = {
			# 'email': '',
			# 'name': '',
			# 'password': '',
		# }
		# response = Client().post('/lab-10/subscribe/', data)
		# json_result = json.loads(response.content.decode('utf-8'))
		# self.assertEqual(json_result['message'] , 'Please write your email correctly.')