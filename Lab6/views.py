from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib import messages
from django.contrib.auth import logout as oauth_logout
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from .models import Status
from .forms import status_form
import datetime, pytz, json, requests

response = {}
def index(request):
    response['author'] = "Adelia Utami"
    stat = Status.objects.all()
    response['stat'] = stat
    html = 'landing_page.html'
    response['stat_form'] = status_form
    return render(request, html, response)

def add_status(request):
    form = status_form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        stat = Status(my_status=request.POST['my_status'])
        stat.save()
        return HttpResponseRedirect('/lab-6/')
    else:
        return HttpResponseRedirect('/lab-6/')
		
# @csrf_exempt	
# def mydiary(request):
	# tgl = request.POST['tgl']
	# catatan = request.POST['catatan']
	# diary = Status.objects.create(tgl = tgl, catatan = catatan)
	# diary.save()
	# json = JsonResponse( {"tgl":tgl,"catatan":catatan}, safe=False )
	# return json
	
def show_profile(request):
    return render(request, "profile.html")

def get_book_list(request):
	get_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
	return HttpResponse(get_json, content_type = 'BookListInJson')	
	
def show_book_list(request):
    return render(request, "book_list.html")
	
def logout(request):
	oauth_logout(request)
	return redirect('/lab-6/book-list/')