from django.db import models
from django import forms
from django.utils import timezone
from datetime import datetime, date

class Status(models.Model):
	my_status = models.TextField(max_length=300)
	time = models.DateTimeField(default=timezone.now)