from django.conf.urls import url
from .views import index, add_status, show_profile, get_book_list, show_book_list, logout

app_name = "Lab6"
urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'my-status/$', add_status, name = 'my-status'),
	url(r'my-profile/$', show_profile, name = 'my-profile'),
	url(r'book-list/$', show_book_list, name = 'book-list'),
	url(r'BookListInJson/$', get_book_list, name = 'book-list-json'),
	url(r'logout/$', logout, name = 'logout'),
	# url(r'data-json/$', get_book_list, name = 'data-json'),
	# url('mydiary/', mydiary, name = 'mydiary'),
	
	
]