from django import forms
from django.forms import ModelForm
from .models import Status

class status_form(forms.ModelForm):
	
	error_messages = {
		'required': 'Please fill this field',
	}
		
	status_attrs = {
		'type': 'text',
		'cols': 50,
		'rows': 4,
		'placeholder':'Type your status here'
	}
	my_status = forms.CharField(label = '', required = True, max_length = 300, widget = forms.Textarea(attrs = status_attrs))

	class Meta:
		model = Status
		fields = ['my_status']