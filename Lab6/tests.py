from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from datetime import datetime, date

from .views import index, add_status, show_profile, show_book_list, get_book_list
from .models import Status
from .forms import status_form

class Lab6UnitTest(TestCase):
	def test_lab_6_status_page_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)	

	def test_lab_6_status_page_using_landing_page_html(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response, 'landing_page.html')	
	
	def test_lab_6_profile_page_using_profile_html(self):
		response = Client().get('/lab-6/my-profile/')
		self.assertTemplateUsed(response, 'profile.html')	
		
	def test_lab_6_list_of_books_page_using_book_list_html(self):
		response = Client().get('/lab-6/book-list/')
		self.assertTemplateUsed(response, 'book_list.html')	

	def test_lab_6_using_index_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)
		
	def test_lab_6_profile_using_show_profile_func(self):
		found = resolve('/lab-6/my-profile/')
		self.assertEqual(found.func, show_profile)	
		
	def test_lab_6_profile_using_show_book_list(self):
		found = resolve('/lab-6/book-list/')
		self.assertEqual(found.func, show_book_list)	
		
	def test_lab_6_profile_using_get_book_list(self):
		found = resolve('/lab-6/BookListInJson/')
		self.assertEqual(found.func, get_book_list)

	def test_models_can_create_new_status(self):
		#Creating a new status
		new_status = Status.objects.create(my_status='Aku mau mendaki gunung lewati lembah sebrangi lautan')

		#Retrieving all available status
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_validation_for_blank_items(self):
		form = status_form(data={'my_status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['my_status'], ["This field is required."])
		
	def test_lab_6_post_success_and_render_the_result(self):
		test = 'Hello'
		response_post = Client().post('/lab-6/my-status/', {'my_status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)	
	
	def test_lab5_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/lab-6/my-status/', {'my_status': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_can_save_a_POST_request(self):
		response = self.client.post('/lab-6/my-status/', data = {'my_status' : 'Aaaaaa'})
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)
		
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/lab-6/')
		
		new_response = self.client.get('/lab-6/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Apa kabar', html_response)