$(document).ready(function() {
	var accordionTitle = $('.accordion-title');
	var accordionText = $('.accordion-text');
	accordionTitle.on('click', function() {
		var thisAccordion = $(this);
		var isActive = thisAccordion.hasClass('active');
		if( isActive ) {
			thisAccordion
				.removeClass('active')
				.parent().find('.accordion-text').slideUp(300);
		} else {
			accordionTitle.removeClass('active');
			accordionText.slideUp(300);
			thisAccordion
				.addClass('active')
				.parent().find('.accordion-text').slideDown(300);
		}
	});
	if($(window).width() >= 1024){
		accordionText.slideUp(300);
	}
	
	
});

$(document).ready(function() {
	$("#theme").click(function(){
		$("#navbarQ").toggleClass("navbar-style-2");
		$("#begron").toggleClass("bg-style-2");
		$("#akord1, #akord2, #akord3").toggleClass("accordion-style-2");
	});
});
	
document.onreadystatechange = function () {
	setTimeout(function(){
		document.getElementById('load').style.visibility="hidden";
		}, 1000);
}

$(document).ready(function() {
	$.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
		type: "get",
		dataType: "json",
		success: function(data) {
			var data_books = "";
			for(i in data.items) {
				data_books += "<tbody><tr>" +
				"<td>"+ data.items[i].volumeInfo.title + "</td>" +
				"<td><img src='"+ data.items[i].volumeInfo.imageLinks.thumbnail + "'></td>" +
				"<td>"+ data.items[i].volumeInfo.description + "</td>" +
				"<td>"+ data.items[i].volumeInfo.authors + "</td>" +
				"<td>"+ data.items[i].volumeInfo.publisher + "</td>" +
				"<td><a class='btn btn-primary' href='#' role='button' id='add-fav'><i class='glyphicon glyphicon-star-empty'></i></a></td>" +	
				"</tr></tbody>"
			}
			$("#book-table").append(data_books);
		}
	})
});

$(document).on('click','#add-fav',function(){
    $('#total-fav').val(parseInt($('#total-fav').val()) + 1 );
});

