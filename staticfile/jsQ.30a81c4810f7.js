$(document).ready(function() {
	var accordionTitle = $('.accordion-title');
	var accordionText = $('.accordion-text');
	accordionTitle.on('click', function() {
		var thisAccordion = $(this);
		var isActive = thisAccordion.hasClass('active');
		if( isActive ) {
			thisAccordion
				.removeClass('active')
				.parent().find('.accordion-text').slideUp(300);
		} else {
			accordionTitle.removeClass('active');
			accordionText.slideUp(300);
			thisAccordion
				.addClass('active')
				.parent().find('.accordion-text').slideDown(300);
		}
	});
	if($(window).width() >= 1024){
		accordionText.slideUp(300);
	}
	
	$("#theme").click(function(){
		$("#navbarQ").toggleClass("navbar-style-2");
		$("#begron").toggleClass("bg-style-2");
		$("#akord1, #akord2, #akord3").toggleClass("accordion-style-2");
	});
	
	setTimeout(function(){
		document.getElementById('load').style.visibility="hidden";
		}, 1000);
});