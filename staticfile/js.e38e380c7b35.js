function subscribe() {
    $.ajax({
        url: "/lab-10/subscribe/",
        type: "POST",
        data: {
            email: $('#email-field').val(),
            nama: $('#name-field').val(),            
            password: $('#password-field').val()
        },
        success: function (json) {
            console.log(json);
            $('#subscribe_form').val(''); // empty form
            $('#response_message').html("<div class='alert alert-success'><strong>Success!</strong>Thank you for subscribing me!</div>")
        },
        error: function (xhr, errmsg, err) {
            $('#response_message').html("<div class='alert alert-danger'><strong>Something goes wrong!</strong></div>");
        },
    });
};

function user_validation() {
    $.ajax({
        url: "/lab-10/validation/",
        type: "POST",
        data: {
            email: $('#email-field').val(),
            nama: $('#name-field').val(),            
            password: $('#password-field').val()
        },

        success: function (response) {
            if (response.message == "All fields are valid!"){
                document.getElementById('sub_btn').disabled = false;
                $('#validation_message').html("<p>"+ response.message + "</p>")
            }
            else {
                document.getElementById('subscribe_button').disabled = true;
                $('#validation_message').html("<p>"+ response.message + "</p>")
            }

        },
    });
};

$(document).ready(function () {
    var x_timer;
    $("#email-field").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {user_validation();}, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#nama-field").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {user_validation();}, 10);

    });
});


$(document).ready(function () {
    var x_timer;
    $("#password-field").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {user_validation();}, 10);
    });
});