function validation() {
    $.ajax({
        url: "/lab-10/validation/",
        type: "POST",
        data: {
            email: $('#email_field').val(),
            name: $('#name_field').val(),
            password: $('#pass_field').val()
        },
        success: function (response) {
            if (response.message == "Your datas are valid!"){
                document.getElementById('sub_btn').disabled = false;
                $('#validation_message').html("<br><div class='alert alert-success' role='alert'>" + response.message + "</div>")
				getData(response);
            }
            else {
                document.getElementById('sub_btn').disabled = true;
                $('#validation_message').html("<br><div class='alert alert-secondary' role='alert'>" + response.message + "</div>")
            }
        },
        error: function (errmsg) {
            alert("Error is happening in validation()");
        }
    });
};

function getData(data) {
	var container = document.getElementById("sub-table");
	res = '<ul>';
		console.log(data.length)
		for (i=0; i<data.length; i++){
			res+='<li> '+ "<button class='btn' onclick='deleteData('+res[i].id + ')'>" + "Unsubscribe" + '</button> '+ data[i].name + ' || ' + data[i].email +  '</li><br>' 
		}
		container.insertAdjacentHTML('beforeend', res + '</ul>')
}

function subscribe() {
    $.ajax({
        url: "/lab-10/subscribe/",
        type: "POST",
        data: {
            email: $('#email_field').val(),
            name: $('#name_field').val(),            
            password: $('#pass_field').val()
        },
        success: function (json) {
            $('#response_message').html("<div class='alert alert-success'><strong>Thank you for subscribing me!^^/</strong></div>")
            $('#validation_message').html('')
        },
        error: function (xhr, errmsg, err) {
            $('#response_message').html("<div class='alert alert-danger'><strong>Errrr, something goes wrong</strong></div>");
            alert("Error is happening in subscribe()");
        },
    });
};

$(document).ready(function () {
    var x_timer;
    $("#email_field").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {validation();}, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#name_field").keyup(function (e) {
        clearTimeout(x_timer);
        var name = $(this).val();
        x_timer = setTimeout(function () {validation();}, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#pass_field").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {validation();}, 10);
    });
});